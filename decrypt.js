var fs = require('fs');
var zlib = require('zlib');
var crypto = require('crypto');

function readByteArray(inputPath, callback) {
    fs.readFile(inputPath, function read(err, data) {
        callback(err, data);
    });
}

function writeByteArray(outputPath, content, callback) {
    fs.writeFile(outputPath, content, 'base64', function written(err) {
        callback(err);
    });
}

function gunzipByteArray(data, callback) {
    zlib.gunzip(data, function (error, result) {
        callback(error, result);
    })
}

function decryptAndDecompressContent(encrypted, callback) {
    var FRAME_SIZE = 8;
    var KEY_SIZE = 8;
    var keyValue = [];

    for (let i = 0; i < KEY_SIZE; i++) {
        var keyIndex = ((i + 1) * 10) - 1;
        keyValue[i] = encrypted[keyIndex];
    }

    var totalBytes = encrypted.length;
    var keyCounter = 0;
    var encodedCounter = 0;
    var xorBytes = new Buffer.alloc(totalBytes - KEY_SIZE);

    for (var encryptedCounter = 0; encryptedCounter < totalBytes; encryptedCounter++) {
        if (encryptedCounter > 0 && encryptedCounter % 10 == 9 && keyCounter < KEY_SIZE){
            //Skip this byte
            keyCounter++;
        } else {
            var xorByte = encrypted[encryptedCounter] ^ keyValue[encodedCounter % FRAME_SIZE];
            xorBytes[encodedCounter] = xorByte;
            encodedCounter++;
        }
    }

    gunzipByteArray(xorBytes, function (err, decompressed) {
        if (err) callback(err, null);

        callback(null, decompressed);
    });
}

function start() {
    var inputPath = './v1-configuration.jsa';
    var outputPath = './config.js';

    readByteArray(inputPath, function (err, encrypted) {
        if (err) return console.log(err);
        decryptAndDecompressContent(encrypted, function (err, decrypted) {
            if (err) return console.log(err);
            writeByteArray(outputPath, decrypted, function (err) {
                if (err) return console.log(err);
                console.log("Decryption: Done and Dusted");
            });
        });
    })
}

start();
